package main

import "github.com/spf13/cobra"

type UserWithStats struct {
	User
	Authored int `json:"authored"`
	Reviewed int `json:"reviewed"`
}

func getUsersCmd(state *[]MergeRequest) *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:   "users",
		Short: "Lists users",
		Long:  "This command lists all users and basic stats on them.",
		RunE: func(_ *cobra.Command, _ []string) error {
			result := map[int]*UserWithStats{}
			for _, mr := range *state {
				if result[mr.Author.ID] == nil {
					result[mr.Author.ID] = &UserWithStats{
						User: mr.Author,
					}
				}

				result[mr.Author.ID].Authored++

				for _, r := range mr.Reviewers {
					if result[r.ID] == nil {
						result[r.ID] = &UserWithStats{
							User: r,
						}
					}

					result[r.ID].Reviewed++
				}
			}

			return Output(Values(result))
		},
	}

	return rootCmd
}
