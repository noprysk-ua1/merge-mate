package main

import (
	"encoding/csv"
	"errors"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

type AlienChartRow struct {
	Time                     string
	UserWithStatsPerUserName map[string]*UserWithStats
}

func getAlienChartCmd(state *[]MergeRequest) *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:   "alien-chart",
		Short: "Show user statistics on authored over time as Alient Charts input",
		Long:  "The command prepares input for https://alienart.io/.",
		RunE: func(_ *cobra.Command, _ []string) error {
			if state == nil || len(*state) == 0 {
				return errors.New("state should not be empty")
			}

			result := []AlienChartRow{}

			userPerUserName := map[string]*UserWithStats{}
			for _, mr := range *state {
				if userPerUserName[mr.Author.Name] == nil {
					userPerUserName[mr.Author.Name] = &UserWithStats{ // Assuming no collisions.
						User: mr.Author,
					}
				}
			}

			prev := AlienChartRow{UserWithStatsPerUserName: userPerUserName}
			for _, mr := range *state { // Already ordered by created_at.
				row := copyRow(prev)
				row.Time = mr.CreatedAt

				row.UserWithStatsPerUserName[mr.Author.Name].Authored++
				result = append(result, row)

				prev = row
			}

			return csvOutput(result)
		},
	}

	return rootCmd
}

func copyRow(row AlienChartRow) AlienChartRow {
	result := AlienChartRow{
		Time:                     row.Time,
		UserWithStatsPerUserName: map[string]*UserWithStats{},
	}

	for name, user := range row.UserWithStatsPerUserName {
		c := *user
		result.UserWithStatsPerUserName[name] = &c
	}

	return result
}

func csvOutput(rows []AlienChartRow) error {
	if len(rows) == 0 {
		return errors.New("rows should not be empty")
	}

	writer := csv.NewWriter(os.Stdout)

	dateTitleHeader := "Date / Title"
	header := []string{dateTitleHeader}
	for name := range rows[0].UserWithStatsPerUserName {
		header = append(header, name)
	}
	if err := writer.Write(header); err != nil {
		return err
	}
	header = header[1:] // Forgetting about the date title.

	values := []string{"Image"} // The first row is special, it only lists images of users.
	for _, key := range header {
		values = append(values, rows[0].UserWithStatsPerUserName[key].AvatarURL)
	}

	writer.Write(values)

	for _, row := range rows {
		values := []string{row.Time}
		for _, key := range header {
			values = append(values, strconv.Itoa(row.UserWithStatsPerUserName[key].Authored))
		}
		if err := writer.Write(values); err != nil {
			return err
		}
	}

	writer.Flush()
	return nil
}
