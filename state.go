package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
)

type MergeRequest struct {
	ID        int    `json:"id"`
	Title     string `json:"title"`
	Author    User   `json:"author"`
	Assignee  User   `json:"assignee"`
	Reviewers []User `json:"reviewers"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	MergedAt  string `json:"merged_at"`
	State     string `json:"state"`
	SHA       string `json:"sha"`
}

type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
}

func ReadState(token string, projectID int, forceReloadState bool) ([]MergeRequest, error) {
	stateFile := fmt.Sprintf("state-%d.json", projectID)

	if forceReloadState {
		_ = os.Remove(stateFile)
	}

	file, err := os.Open(stateFile)
	if err == nil {
		defer file.Close()
		var result []MergeRequest
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&result)
		if err == nil {
			return result, nil
		}
	}

	result, err := readState(token, projectID, "1")
	if err != nil {
		return nil, err
	}

	file, err = os.Create(stateFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	if err := encoder.Encode(result); err != nil {
		return nil, err
	}

	return result, nil
}

func readState(token string, projectID int, page string) ([]MergeRequest, error) {
	if page == "" {
		return nil, nil
	}

	u, err := url.Parse(fmt.Sprintf("https://gitlab.com/api/v4/projects/%d/merge_requests", projectID))
	if err != nil {
		return nil, err
	}

	q := u.Query()
	q.Set("state", "merged")
	q.Set("scope", "all")
	q.Set("per_page", "100")
	q.Set("page", page)
	q.Set("target_branch", "master")
	q.Set("order_by", "created_at")
	q.Set("sort", "asc")
	u.RawQuery = q.Encode()

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("PRIVATE-TOKEN", token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		return nil, fmt.Errorf("calling GitLab API returned status %s: %s", resp.Status, buf.String())
	}

	var result []MergeRequest
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}

	fmt.Printf("listed %d merge requests at page %s\n", len(result), page)

	other, err := readState(token, projectID, resp.Header.Get("X-Next-Page"))
	if err != nil {
		return nil, err
	}

	return append(result, other...), nil
}
