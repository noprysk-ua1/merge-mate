package main

import (
	"errors"
	"sort"

	"github.com/spf13/cobra"
)

type UserWithMates struct {
	User
	ReviewedBy []Mate `json:"reviewed_by"`
	Reviewed   []Mate `json:"reviewed"`
}

type Mate struct {
	User
	Count int `json:"count"`
}

func getMatesCmd(state *[]MergeRequest) *cobra.Command {
	var (
		id    int
		limit int
	)

	var rootCmd = &cobra.Command{
		Use:   "mates",
		Short: "Lists mates for users",
		Long:  "This command top collaborators in merge requests for each user.",
		RunE: func(_ *cobra.Command, _ []string) error {
			if limit < 1 {
				return errors.New("--limit should be a positive number")
			}

			result := []UserWithMates{}

			userPerUserID := map[int]User{}
			for _, mr := range *state {
				if _, ok := userPerUserID[mr.Author.ID]; !ok {
					userPerUserID[mr.Author.ID] = mr.Author
				}

				for _, reviewer := range mr.Reviewers {
					if _, ok := userPerUserID[reviewer.ID]; !ok {
						userPerUserID[reviewer.ID] = reviewer
					}
				}
			}

			reviewedByCount := map[int]map[int]int{}
			reviewedCount := map[int]map[int]int{}
			for _, mr := range *state {
				for _, reviewer := range mr.Reviewers {
					if reviewedByCount[mr.Author.ID] == nil {
						reviewedByCount[mr.Author.ID] = map[int]int{}
					}

					reviewedByCount[mr.Author.ID][reviewer.ID]++

					if reviewedCount[reviewer.ID] == nil {
						reviewedCount[reviewer.ID] = map[int]int{}
					}

					reviewedCount[reviewer.ID][mr.Author.ID]++
				}
			}

			for _, u := range userPerUserID {
				if id != 0 && u.ID != id {
					continue
				}

				userWithMates := UserWithMates{User: u}

				for id, count := range reviewedByCount[u.ID] {
					userWithMates.ReviewedBy = append(userWithMates.ReviewedBy, Mate{
						User:  userPerUserID[id],
						Count: count,
					})
				}

				for id, count := range reviewedCount[u.ID] {
					userWithMates.Reviewed = append(userWithMates.Reviewed, Mate{
						User:  userPerUserID[id],
						Count: count,
					})
				}

				result = append(result, userWithMates)
			}

			for i, u := range result {
				sort.Slice(u.Reviewed, func(i, j int) bool {
					return u.Reviewed[i].Count > u.Reviewed[j].Count
				})

				sort.Slice(u.ReviewedBy, func(i, j int) bool {
					return u.ReviewedBy[i].Count > u.ReviewedBy[j].Count
				})

				if len(result[i].Reviewed) > limit {
					result[i].Reviewed = u.Reviewed[0:limit]
				}
				if len(result[i].ReviewedBy) > limit {
					result[i].ReviewedBy = u.ReviewedBy[0:limit]
				}
			}

			return Output(result)
		},
	}

	rootCmd.Flags().IntVar(&id, "id", 0, "GitLab user ID")
	rootCmd.Flags().IntVar(&limit, "limit", 5, "Limit of mates to display")

	return rootCmd
}
