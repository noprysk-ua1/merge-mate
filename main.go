package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func main() {
	var (
		token            string
		projectID        int
		state            []MergeRequest
		forceReloadState bool
	)

	var rootCmd = &cobra.Command{
		Use:   "merge-mate",
		Short: "Merge Mate",

		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			if token == "" {
				token = os.Getenv("GITLAB_TOKEN")
			}

			if token == "" {
				return errors.New("either indicate the --token flag with the GitLab private token or set the GITLAB_TOKEN environmental variable")
			}

			if projectID == 0 {
				return errors.New("set the --project-id flag equal to the ID of the GitLab project of interest")
			}

			var err error
			state, err = ReadState(token, projectID, forceReloadState)
			if err != nil {
				return err
			}

			return nil
		},

		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("successfully found data on %d merge requests\n", len(state))
		},
	}

	rootCmd.PersistentFlags().StringVarP(&token, "token", "t", "", "GitLab private token")
	rootCmd.PersistentFlags().IntVarP(&projectID, "project-id", "p", 0, "GitLab project ID")
	rootCmd.PersistentFlags().BoolVarP(&forceReloadState, "force-reload-state", "f", false, "Force reload the cached state of merge requests")

	rootCmd.AddCommand(getUsersCmd(&state))
	rootCmd.AddCommand(getAlienChartCmd(&state))
	rootCmd.AddCommand(getMatesCmd(&state))
	rootCmd.AddCommand(getGephiCmd(&state))

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
	}
}
