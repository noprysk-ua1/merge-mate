package main

import (
	"encoding/json"
	"fmt"
)

func Output(e interface{}) error {
	result, err := json.MarshalIndent(e, "", "  ")
	if err != nil {
		return err
	}

	fmt.Println(string(result))
	return nil
}

func Values[A comparable, B any](m map[A]B) []B {
	result := make([]B, 0, len(m))
	for _, v := range m {
		result = append(result, v)
	}

	return result
}
