package main

import (
	"encoding/csv"
	"errors"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

type Gephi struct {
	NodesPath string `json:"nodes_path"`
	EdgesPath string `json:"edges_path"`
}

func getGephiCmd(state *[]MergeRequest) *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:   "gephi",
		Short: "Show mate statistics on merge requests as Gephi input",
		Long:  "The command prepares input for https://gephi.org/.",
		RunE: func(_ *cobra.Command, _ []string) error {
			result := Gephi{}

			userPerUserID := map[int]User{}
			for _, mr := range *state {
				if _, ok := userPerUserID[mr.Author.ID]; !ok {
					userPerUserID[mr.Author.ID] = mr.Author
				}

				for _, reviewer := range mr.Reviewers {
					if _, ok := userPerUserID[reviewer.ID]; !ok {
						userPerUserID[reviewer.ID] = reviewer
					}
				}
			}

			path, err := createNodesFile(userPerUserID)
			if err != nil {
				return err
			}
			result.NodesPath = path

			reviewedCount := map[int]map[int]int{}
			for _, mr := range *state {
				for _, reviewer := range mr.Reviewers {
					if reviewedCount[reviewer.ID] == nil {
						reviewedCount[reviewer.ID] = map[int]int{}
					}

					reviewedCount[reviewer.ID][mr.Author.ID]++
				}
			}

			path, err = createEdgesFile(reviewedCount)
			if err != nil {
				return err
			}
			result.EdgesPath = path

			return Output(result)
		},
	}

	return rootCmd
}

func createNodesFile(userPerUserID map[int]User) (path string, err error) {
	if userPerUserID == nil {
		err = errors.New("users should not be empty")
		return
	}

	var file *os.File
	file, err = os.CreateTemp("", "*-nodes.csv")
	if err != nil {
		return
	}

	writer := csv.NewWriter(file)
	defer func() {
		writer.Flush()

		cerr := file.Close() // Closing only after flushing the writer.
		if cerr != nil {
			err = cerr
		}
	}()

	header := []string{"Id", "Label"}
	err = writer.Write(header)
	if err != nil {
		return
	}

	for id, user := range userPerUserID {
		err = writer.Write([]string{
			strconv.Itoa(id), // Id.
			user.Username,    // Label.
		})
		if err != nil {
			return
		}
	}

	path = file.Name()
	return
}

func createEdgesFile(reviewedCount map[int]map[int]int) (path string, err error) {
	if reviewedCount == nil {
		err = errors.New("reviews should not be empty")
		return
	}

	var file *os.File
	file, err = os.CreateTemp("", "*-edges.csv")
	if err != nil {
		return
	}

	writer := csv.NewWriter(file)
	defer func() {
		writer.Flush()

		cerr := file.Close() // Closing only after flushing the writer.
		if cerr != nil {
			err = cerr
		}
	}()

	header := []string{"Source", "Target", "Weight"}
	err = writer.Write(header)
	if err != nil {
		return
	}

	for source, targets := range reviewedCount {
		for target, count := range targets {
			err = writer.Write([]string{
				strconv.Itoa(source), // Source.
				strconv.Itoa(target), // Target.
				strconv.Itoa(count),  // Weight.
			})
			if err != nil {
				return
			}
		}
	}

	path = file.Name()
	return
}
