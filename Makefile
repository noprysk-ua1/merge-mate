BINARY_NAME=merge-mate

default: build

deps:
	go mod tidy

build: deps
	go build -o $(BINARY_NAME)

clean:
	rm -f $(BINARY_NAME)